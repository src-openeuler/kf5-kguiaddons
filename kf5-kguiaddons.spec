%undefine __cmake_in_source_build
%global framework kguiaddons

Name:       kf5-%{framework}
Version:    5.116.0
Release:    1
Summary:    KDE Frameworks 5 Tier 1 addon with various classes on top of QtGui

License:    GPLv2+ and LGPLv2+
URL:        https://invent.kde.org/frameworks/%{framework}

%global majmin %majmin_ver_kf5
%global stable %stable_kf5

Source0:        https://download.kde.org/%{stable}/frameworks/%{majmin}/%{framework}-%{version}.tar.xz

BuildRequires:  extra-cmake-modules >= %{majmin}
BuildRequires:  kf5-rpm-macros >= %{majmin}
BuildRequires:  libX11-devel
BuildRequires:  libxcb-devel
BuildRequires:  plasma-wayland-protocols-devel
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qtbase-private-devel
BuildRequires:  qt5-qtwayland-devel pkgconfig(wayland-client)
BuildRequires:  qt5-qtx11extras-devel

Requires:       kf5-filesystem >= %{majmin}

%description
KDBusAddons provides convenience classes on top of QtGui.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt5-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -n %{framework}-%{version}

%build
%{cmake_kf5}

%cmake_build

%install
%cmake_install

%ldconfig_scriptlets

%files
%doc README.md
%license LICENSES/*.txt
%{_kf5_bindir}/kde-geo-uri-handler
%{_kf5_datadir}/qlogging-categories5/*categories
%{_kf5_libdir}/libKF5GuiAddons.so.*
%{_kf5_datadir}/applications/*-handler.desktop

%files devel
%{_kf5_includedir}/KGuiAddons/
%{_kf5_libdir}/libKF5GuiAddons.so
%{_kf5_libdir}/cmake/KF5GuiAddons/
%{_kf5_archdatadir}/mkspecs/modules/qt_KGuiAddons.pri

%changelog
* Wed Jan 15 2025 misaka00251 <liuxin@iscas.ac.cn> - 5.116.0-1
- Update package to version 5.116.0

* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 5.115.0-2
- adapt to the new CMake macros to fix build failure

* Fri Mar 01 2024 maqi <maqi@uniontech.com> - 5.115.0-1
- Update package to version 5.115.0

* Wed Jan 03 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 5.113.0-1
- Update package to version 5.113.0

* Thu Aug 03 2023 wangqia <wangqia@uniontech.com> - 5.108.0-1
- Update to upstream version 5.108.0

* Fri Dec 09 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 5.100.0-1
- Update to upstream version 5.100.0

* Mon Sep 05 2022 liweiganga <liweiganga@uniontech.com> - 5.97.0-1
- update to upstream version 5.97.0

* Tue Jul 05 2022 loong_C <loong_c@yeah.net> - 5.95.0-1
- update to upsttream version 5.95.0

* Sat Feb 12 2022 tanyulong <tanyulong@kylinos.cn> - 5.90.0-1
- update to upstream version 5.90.0

* Thu Jan 13 2022 pei-jiankang <peijiankang@kylinos.cn> - 5.88.0-1
- update to upstream version 5.88.0

* Mon Dec 13 2021 douyan <douyan@kylinos.cn> - 5.55.0-2
- remove %{?dist}

* Mon Feb 04 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.55.0-1
- 5.55.0

* Thu Jul 23 2020 wangmian <wangmian@kylinos.cn> - 5.55.0-1
- Init kf5-kguiaddons project
